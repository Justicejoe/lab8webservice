/*Multithreading
 1.) UI thread
 2.) Background thread <----- AsyncTask */

package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String province = "bangkok";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// do not allow the layout to be rotate base on the orientation of the
		// device if rotate it will interrupt with the background thread so just
		// fix it and set it to can't rotate
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String, String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[] {
				"name", "value" }, new int[] { R.id.tvName, R.id.tvValue });
		setListAdapter(adapter);
		
		File infile = getBaseContext().getFileStreamPath("contact.tsv");
		if (infile.exists()) {
			try {
				Scanner sc = new Scanner(infile);
				while (sc.hasNextLine()) {
					province = sc.nextLine();
					
				}
				sc.close();
			} catch (FileNotFoundException e) {
				// Do nothing
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		reload(5,province);
	}

	public void reload(int T,String P)
	{
		try {
			FileOutputStream outfile = openFileOutput("contact.tsv",
					MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(P);
			p.flush();
			p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		// Check if the device is connected to a network
				ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo info = mgr.getActiveNetworkInfo();
				if (info != null && info.isConnected()) {
					// the device is connected to a network
					// Load data
					// Before loading, we compare the current time and the last update
					// If it's longer then 5 minutes since the last update, we load the
					// new data
					long current = System.currentTimeMillis();
					if (current - lastUpdate > T * 60 * 1000) { // make millisec to a
																// minute
						// We start an AsyncTask for loading data
						WeatherTask task = new WeatherTask(this);
						task.execute("http://cholwich.org/"+ P +".json");
						// "http://ict.siit.tu.ac.th/~cholwich/bangkok.son"
					}
				} else {
					Toast t = Toast
							.makeText(
									this,
									"No Internet Connectivity on this device. Check your setting",
									Toast.LENGTH_LONG);
					t.show();
				}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (outState != null) {
			super.onSaveInstanceState(outState);
		}
		
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_refresh:
			reload(3,province);
			break;
		case R.id.shutdownbangkok:
			province = "bangkok";
			reload(0,province);
			break;
			
		case R.id.nonthaburi:
			province = "nonthaburi";
			reload(0,province);
			break;
			
		case R.id.pathumthani:
			province = "pathumthani";
			reload(0,province);
			break;
		default:
			return super.onOptionsItemSelected(item);

		}
		return true;
	}
	
	
	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		// Executed under the UI thread before the task start
		@Override
		protected void onPreExecute() { // execute before the task start
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		// Executed under the UI thread after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), result,
					Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis(); // keep the last time
														// updating the data
			// Set the title of the MainActivity
			setTitle(Character.toUpperCase(province.charAt(0))+province.substring(1, province.length()) + " Weather");
		}

		// Executed under the background thread
		@Override
		protected String doInBackground(String... params) {// ... mean array
															// accept any number
															// of data in the
															// form of string
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				// Get the first parameter string, and use it as a URL
				URL url = new URL(params[0]);
				// Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection) url
						.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				// Read data from the webserver
				http.setDoInput(true); // if want to put the data to the server
										// use DoOutput
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);

					// Extract "description"
					// "weather": [{"description": "....."} ]
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w1 = jweather.getJSONObject(0);
					String description = w1.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);

					Double pressure = jmain.getDouble("pressure");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f", pressure));
					list.add(record);

					Double humidity = jmain.getDouble("humidity");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f", humidity));
					list.add(record);

					Double temp_min = jmain.getDouble("temp_min") - 273.0;
					record = new HashMap<String, String>();
					record.put("name", "Temperature Min");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f", temp_min));
					list.add(record);

					Double temp_max = jmain.getDouble("temp_max") - 273.0;
					record = new HashMap<String, String>();
					record.put("name", "Temperature Max");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f", temp_max));
					list.add(record);

					JSONObject jwind = json.getJSONObject("wind");
					double windspeed = jwind.getDouble("speed");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f", windspeed));
					list.add(record);

					double winddeg = jwind.getDouble("deg");
					record = new HashMap<String, String>();
					record.put("name", "Wind Deg");
					record.put("value",
							String.format(Locale.getDefault(), "%.1f", winddeg));
					list.add(record);

					return "Finished Loading Weather Data";
				} else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}

// JSON format
// { } --> object
// [ ] --> array
//
// {"firstName" : "Cholwich"}
// firstname --> column name
// Cholwich --> value

/*
 * ["a","b"] a --> 0 b --> 1
 * 
 * object > names > array 0-> John 1-> Peter
 */

